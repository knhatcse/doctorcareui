import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {Face} from './FaceContainer';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import LinearGradient from 'react-native-linear-gradient';
const Icon = ({Wrapper = AntDesign, icon, title, color}) => {
  return (
    <View style={styles.item}>
      <Wrapper name={icon} color={color} size={36} />

      <Text style={[styles.title, {color}]}>{title}</Text>
    </View>
  );
};

const Rating = ({rating}) => {
  return (
    <View style={styles.rating}>
      {Array(5)
        .fill(0)
        .map((_, i) => {
          if (rating > i) {
            return <AntDesign name="star" color="#F1962D" />;
          }
          return <AntDesign name="staro" color="#222" />;
        })}
    </View>
  );
};

const CardHome = ({title, info, noHeader, noFooter, book}) => {
  return (
    <View style={styles.container}>
      {!noHeader && (
        <View style={styles.containerHeading}>
          <Text style={styles.heading}>{title}</Text>
          <Text style={styles.seeall}>See all</Text>
        </View>
      )}
      <View style={styles.card}>
        <View style={styles.cardHeader}>
          <Image style={styles.img} source={require('../assets/user.jpg')} />
          <View style={styles.cardContent}>
            {info.tag && info.tag.length > 0 && (
              <Text style={styles.cardTag}>{info.tag}</Text>
            )}
            <Text style={styles.cardName}>{info.name}</Text>
            <Text style={styles.cardAdress}>{info.address}</Text>
            <Text style={styles.cardDetail}>{info.position}</Text>
            <Text style={styles.cardDetail}>{info.detail}</Text>
            {info.rating > 0 && <Rating rating={info.rating} />}
            <View style={styles.cardMore}>
              <AntDesign name="right" size={20} />
            </View>
            {info.checked && (
              <View style={styles.cardLike}>
                <AntDesign name="heart" size={20} color="#BF3F88" />
              </View>
            )}
            {book && (
              <View style={styles.containerBtn}>
                <LinearGradient
                  start={{x: 0.0, y: 0}}
                  end={{x: 1, y: 0}}
                  colors={['#4F3977', '#673778', '#86357A']}
                  style={[styles.btnBook]}>
                  <Text style={styles.bookText}>Book Visit</Text>
                </LinearGradient>
              </View>
            )}
          </View>
        </View>
        {!noFooter && <View style={styles.line} />}
        {!noFooter && (
          <View style={styles.cardFooter}>
            <Icon icon="checkcircleo" title="Check-in" />
            <Icon icon="closecircleo" title="Cancel" />
            <Icon icon="calendar" title="Calendar" />
            <Icon icon="explore" title="Directions" Wrapper={MaterialIcons} />
          </View>
        )}
      </View>
    </View>
  );
};

export default CardHome;

const styles = StyleSheet.create({
  containerBtn: {
    flexDirection: 'row',
    marginTop: 10,
  },
  btnBook: {
    padding: 10,
    borderRadius: 50,
    paddingHorizontal: 20,
  },
  bookText: {
    fontWeight: 'bold',
    color: '#fff',
  },
  container: {
    padding: 15,
  },
  item: {justifyContent: 'center', alignItems: 'center'},
  img: {
    width: 60,
    height: 60,
    borderRadius: 60,
    backgroundColor: 'gray',
  },
  line: {
    backgroundColor: '#EEEEEF',
    height: 1.5,
    marginVertical: 5,
  },
  containerHeading: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 15,
  },
  heading: {
    fontSize: 24,
    fontWeight: 'bold',
  },

  seeall: {
    fontWeight: 'bold',
    color: '#8075A0',
  },

  cardMore: {
    position: 'absolute',
    bottom: -2,
    right: -2,
  },
  cardLike: {
    position: 'absolute',
    top: -2,
    right: -2,
  },
  card: {
    // marginTop: 5,
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 10,
    borderRadius: 20,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.2,
    shadowRadius: 3.84,
  },
  cardHeader: {
    flexDirection: 'row',
    padding: 10,
  },

  title: {
    marginTop: 5,
    fontWeight: '500',
  },

  cardName: {
    color: '#222',
    fontSize: 18,
    fontWeight: 'bold',
  },

  cardAdress: {
    marginTop: 5,
    color: '#222',
    fontSize: 16,
    fontWeight: '500',
  },

  cardContent: {
    paddingHorizontal: 10,
    flex: 1,
  },

  cardDetail: {
    marginTop: 5,
    color: 'gray',
    fontSize: 15,
    fontWeight: '500',
  },

  cardFooter: {
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  rating: {
    flexDirection: 'row',
    marginTop: 5,
  },

  cardTag: {
    fontSize: 13,
    fontWeight: 'bold',
    color: '#AB7BA9',
  },
});
