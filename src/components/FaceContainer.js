import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Fontisto from 'react-native-vector-icons/Fontisto';

const Face = ({icon, title, color, bold}) => {
  return (
    <View style={styles.item}>
      {bold ? (
        <View style={{backgroundColor: color, borderRadius: 40}}>
          <Fontisto name={icon} color={'#fff'} size={36} />
        </View>
      ) : (
        <Fontisto name={icon} color={color} size={36} />
      )}

      <Text style={[styles.title, {color}]}>{title}</Text>
    </View>
  );
};

const FaceContainer = () => {
  return (
    <View style={styles.container}>
      <Face icon="laughing" title="Greet" color="#DA60AA" />
      <Face icon="slightly-smile" title="Good" color="#DA60AA" />
      <Face icon="neutral" title="Okey" color="#8A3E85" bold />
      <Face icon="frowning" title="Bad" color="#553C8A" />
      <Face icon="expressionless" title="Awful" />
    </View>
  );
};

export default FaceContainer;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 20,
    borderRadius: 20,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.2,
    shadowRadius: 3.84,
  },
  title: {
    fontSize: 16,
    marginTop: 6,
  },
  item: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
