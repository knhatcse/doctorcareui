import React from 'react';
import {StyleSheet, View, TouchableOpacity} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';

const ButtonIcon = ({icon, selected, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <AntDesign name={icon} size={36} color={selected ? '#824F91' : '#222'} />
    </TouchableOpacity>
  );
};

const menus = ['home', 'search1', 'hearto', 'calendar', 'user'];

const BottomBar = ({onSelected, selected}) => {
  return (
    <View style={styles.bottoms}>
      {menus.map((e, i) => {
        return (
          <ButtonIcon
            key={e}
            onPress={() => onSelected(i)}
            icon={e}
            selected={selected == i}
          />
        );
      })}
    </View>
  );
};

export default BottomBar;

const styles = StyleSheet.create({
  bottoms: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    padding: 20,
    justifyContent: 'space-between',
    paddingHorizontal: 30,
    paddingBottom: 20 + 22,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.2,
    shadowRadius: 3.84,
  },
});
