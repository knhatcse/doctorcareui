import React from 'react';
import {StyleSheet, Dimensions, Text, View} from 'react-native';
import CardHome from './components/CardHome';
import FaceContainer from './components/FaceContainer';
const W = Dimensions.get('window').width;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  heading: {
    fontSize: 32,
    fontWeight: 'bold',
    color: '#fff',
  },
  desc: {
    fontSize: 20,
    fontWeight: '400',
    color: '#fff',
  },
  containerHeader: {
    padding: 20,
    paddingHorizontal: 30,
    marginTop: 52,
  },
});
const HomeScreen = () => {
  return (
    <View style={styles.container}>
      <View style={styles.containerHeader}>
        <Text style={styles.heading}>Hi Carly</Text>
        <Text style={styles.desc}>How are you feeling today ?</Text>
      </View>

      <FaceContainer />
      <View>
        <CardHome
          title="Your Next Appointment"
          info={{
            name: 'Dr T Pay Dhar',
            address: 'Sunday, May 15th at 8:00 PM',
            position: '570 Kemmer Shores',
            detail: 'San Francisco, CA 90293',
          }}
        />
        <CardHome
          title="Specialist in your area"
          info={{
            name: 'Dr Ayon Das',
            address: 'Popular Pharma Limited',
            position: 'Dermatologists',
            detail: 'San Francisco, CA | 5 min',
            checked: true,
            tag: 'Wellness',
            rating: 4,
          }}
        />
      </View>
    </View>
  );
};

export default HomeScreen;
