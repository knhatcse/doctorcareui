import React from 'react';
import {StyleSheet, Text, View, TextInput, ImageBackground} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import CardHome from './components/CardHome';
const Header = () => {
  return (
    <View style={styles.searchContainer}>
      <AntDesign name="left" size={24} color="#fff" />
      <View style={styles.headerSearch}>
        <Text style={styles.searchText}>Search</Text>
        <View style={styles.listbutton}>
          <Entypo name="map" size={30} color="#fff" />
          <Entypo style={{marginLeft: 10}} name="list" size={30} color="#fff" />
        </View>
      </View>
      <View style={styles.listFormSearch}>
        <View style={styles.inputForm}>
          <AntDesign name="search1" size={18} color="gray" />
          <TextInput style={styles.input} value="Dermatologists" />
        </View>
        <View style={styles.inputForm}>
          <Feather name="map-pin" size={18} color="gray" />
          <TextInput
            style={[styles.input, {color: '#81578E'}]}
            value="Current Location"
          />
          <Text style={styles.more}>12 ml</Text>
        </View>
      </View>
    </View>
  );
};

const Map = () => {
  return (
    <ImageBackground style={styles.map} source={require('./assets/map.png')}>
      <View style={styles.zoomIcon}>
        <MaterialIcons name="zoom-out-map" size={30} />
      </View>
      <View style={styles.markerWrapper}>
        <View style={[styles.shadowColor, {backgroundColor: '#F6EFF5'}]}>
          <View style={styles.shadowColor}>
            <View style={styles.marker}>
              <FontAwesome5 name="user" size={12} color="#fff" />
            </View>
          </View>
        </View>
      </View>
    </ImageBackground>
  );
};

const SearchScreen = () => {
  return (
    <View>
      <Header />
      <Map />
      <View>
        <CardHome
          noHeader
          noFooter
          book
          info={{
            name: 'Dr Ayon Das',
            address: 'Popular Pharma Limited',
            position: 'Dermatologists',
            detail: 'San Francisco, CA | 5 min',
            checked: true,
            tag: 'Wellness',
            rating: 4,
          }}
        />
        <CardHome
          noHeader
          noFooter
          book
          info={{
            name: 'Dr Ghmagiya Sen',
            address: 'Popular Pharma Limited',
            position: 'Dermatologists',
            detail: 'San Francisco, CA | 5 min',
            checked: true,
            tag: 'Wellness',
            rating: 4,
          }}
        />
      </View>
    </View>
  );
};

export default SearchScreen;

const styles = StyleSheet.create({
  zoomIcon: {
    backgroundColor: '#fff',
    padding: 8,
    position: 'absolute',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.2,
    shadowRadius: 3.84,
    borderRadius: 8,
    bottom: 10,
    right: 10,
  },
  listbutton: {
    flexDirection: 'row',
  },
  searchContainer: {
    padding: 15,
    marginTop: 15 + 24,
  },
  searchText: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#fff',
  },
  headerSearch: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  listFormSearch: {
    marginTop: 10,
  },
  inputForm: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 6,
    alignItems: 'center',
    marginBottom: 12,
  },
  input: {
    marginLeft: 10,
    fontSize: 18,
    flex: 1,
  },
  more: {
    fontSize: 18,
    color: 'gray',
  },

  map: {
    height: 250,
  },
  marker: {
    backgroundColor: '#804A86',

    padding: 5,
    borderRadius: 20,
    shadowColor: '#804A86',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.2,
    shadowRadius: 3.84,
  },

  shadowColor: {
    backgroundColor: '#DCCBD9',
    padding: 5,
    borderRadius: 40,
  },

  markerWrapper: {
    position: 'absolute',

    left: 200,
    top: 100,
  },
});
