/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
  Dimensions,
} from 'react-native';
import HeaderBackground from './src/components/HeaderBackground';

import BottomBar from './src/components/BottomBar';
import HomeScreen from './src/HomeScreen';
import SearchScreen from './src/SearchScreen';
const W = Dimensions.get('window').width;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    backgroundColor: '#F0F0F0',
    // paddingTop: 34,
  },
  bgImg: {
    position: 'absolute',
    width: W,
    height: 220,
  },

  bgImg2: {
    position: 'absolute',
    width: W,
    height: 240,
    borderRadius: 0,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
  },

  body: {
    flex: 1,
  },
});

const App = () => {
  const [tab, setTab] = useState(0);
  const getContent = () => {
    if (tab == 0) {
      return <HomeScreen />;
    }

    if (tab == 1) {
      return <SearchScreen />;
    }

    return null;
  };
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <View style={styles.container}>
        <HeaderBackground style={[tab == 0 ? styles.bgImg : styles.bgImg2]} />
        <ScrollView style={styles.body}>{getContent()}</ScrollView>
        <BottomBar selected={tab} onSelected={i => setTab(i)} />
      </View>
    </>
  );
};

export default App;
